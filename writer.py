#!/usr/bin/env python3

from PIL import ImageFont, ImageDraw, Image

if __name__ == '__main__' :
    fnt = ImageFont.truetype('font.ttf', 70)
    colors = [
        (250, 250, 250, 255),
        (242, 27, 28, 255)
    ]

    blank_image = Image.new('RGBA', (500, 78))
    d = ImageDraw.Draw(blank_image)
    d.text((4, 0), "SPEEDRUN", font=fnt, fill=(colors[0]))
    d.text((330, 0), ".COM", font=fnt, fill=colors[1])

    blank_image.save('logo.png')
