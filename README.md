#  Brawl Speedrun Logo
Script to generate a logo for speedrun.com

![](logo.png)

## Requirements
Have python3 and install PIL with pip
```
pip3 install -r requirements.txt
```
